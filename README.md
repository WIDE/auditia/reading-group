# Auditia Reading Group

![Lisez auditia !](affiche-rg.png)

The **objective** of this reading group is to *informally* share papers. 
There is no good or bad reason to share a paper : it may be because it is linked to our research interests, 
a random paper we particularly appreciated, a paper presenting a fun puzzle, a paper we want to rant about,
a paper we did not understand, an old key paper of the field, a new shiny clickbait paper, a paper we wrote, 
in a nutshell *any* paper.

- The reading group is held **every two weeks** on **Tuesday** at **3PM**, it is **at most 1 hour long**. 
- Papers are suggested in the paper suggestion [issue](#25), people vote on the papers they want to have in the reading goup.
- Each week, the most upvoted paper is presented (by the person who suggested it or anyone who read it)
- Links to read papers are archived in the file `{year}.md`

## How to suggest/vote for a paper ?

Everything is explained at the head of [issue](#25) !

## People involved

- [Jade GARCIA BOURREE](), PhD student @ [INRIA WIDE](https://team.inria.fr/wide/)
- [Augustin GODINOT](https://grodino.github.io), PhD student @ [INRIA WIDE](https://team.inria.fr/wide/)
- [Erwan LE MERRER](https://erwanlemerrer.github.io/), Researcher @ [INRIA WIDE](https://team.inria.fr/wide/)
- [Camilla PENZO](), Data Scientist @ [PEReN](https://www.peren.gouv.fr/)
- [Benoit ROTTEMBOURG](https://twitter.com/benrbg), ? @ [INRIA REGALIA](https://inria.fr)
- [François TAÏANI](https://team.inria.fr/wide/team/francois-taiani/), Professor (Team Lead) @ [INRIA WIDE](https://team.inria.fr/wide/)
- [Gilles TREDAN](https://homepages.laas.fr/gtredan/), Researcher @ [CNRS LAAS](https://laas.fr/)
